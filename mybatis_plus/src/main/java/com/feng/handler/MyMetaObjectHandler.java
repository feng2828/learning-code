package com.feng.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author Feng
 * @Date 2021/11/19 14:13
 * @Version 1.0
 * @Description
 */
@Slf4j
@Component //注意不要忘记注册组件
public class MyMetaObjectHandler implements MetaObjectHandler {

    //插入策略
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("插入填充。。。。。。。");
        //设置字段的值
        this.setFieldValByName("createTime",new Date(),metaObject);
        this.setFieldValByName("updateTime",new Date(),metaObject);
    }

    // 更新操作的策略
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("更新填充。。。。。。。");
        this.setFieldValByName("updateTime",new Date(),metaObject);
    }
}
