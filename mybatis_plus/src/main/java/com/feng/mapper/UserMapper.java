package com.feng.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.feng.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author Feng
 * @Date 2021/11/18 10:07
 * @Version 1.0
 * @Description
 */
@Repository //代表持久层,仓库的意思
// @Mapper
public interface UserMapper extends BaseMapper<User> {

}
