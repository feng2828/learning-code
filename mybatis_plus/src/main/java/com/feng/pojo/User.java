package com.feng.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author Feng
 * @Date 2021/11/18 10:00
 * @Version 1.0
 * @Description 实体类User
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {

    @TableId(type = IdType.AUTO)
    private Long id;
    private String name;
    private Integer age;
    private String email;

    @Version // mp的乐观锁Version注解
    private Integer version;

    @TableLogic//逻辑删除注解
    private Integer deleted;

    // 字段在什么时候填充内容
    @TableField(fill = FieldFill.INSERT) //插入时填充
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE) //插入和更新的时候填充
    private Date updateTime;
}
